-- Aufgaben zu Aggregatsfunktionen 
-- a.	Welches ist das niedrigste/höchste Gehalt eines Lehrers?
select min(gehalt),max(gehalt) from Lehrer;
-- b.	Was ist das niedrigste Gehalt, das einer unserer Mathelehrer bekommt?
select min(gehalt) from lehrer join lehrer_hat_faecher on lehrer.id = lehrer_hat_faecher.idLehrer
join faecher on lehrer_hat_faecher.idFaecher = faecher.id
where faecher.fachbezeichnung = 'Mathe'; 
-- c.	Was ist der beste Notendurchschnitt der Noten Deutsch/Mathe?
select max((noteDeutsch + noteMathe)/2) from schueler; -- wieso geht das nicht
-- d.	Wie viel Einwohner hat der größte Ort, wie viel der Kleinste? Ausgabe "Höchste Einwohnerzahl", "Niedrigste Einwohnerzahl"
select max(einwohnerzahl) As hoechste, min(einwohnerzahl) as niedrigste from orte;
SELECT MIN(orte.einwohnerzahl) AS 'Niedrigste Einwohnerzahl', MAX(orte.einwohnerzahl) as 'Höchste Einwohnerzahl'
FROM orte;
-- e.	Wie groß ist die Differenz zwischen dem Ort mit den meisten und dem mit den wenigsten Einwohnern (z.B.: kleinster Ort hat 
-- 1000 Einwohner, größter Ort hat 3000 - Differenz ist 2000). Ausgabe einer Spalte "Differenz".
select max(einwohnerzahl) As hoechste, min(einwohnerzahl) as niedrigste, max(einwohnerzahl) - min(einwohnerzahl) as differenz from orte;
-- f.	Wie viele Schüler haben wir in der Datenbank?
select count(id) from schueler;
-- g.	Wie viele Schüler haben ein Smartphone?
select count(id) from schueler where idSmartphones is not null;
-- h.	Wie viele Schüler haben ein Smartphone der Firma Samsung oder der Firma HTC?
select count(schueler.id) from schueler join smartphones
on smartphones.id = schueler.idSmartphones where smartphones.marke = 'Samsung' or smartphones.marke ='HTC';
-- i.	Wie viele Schüler wohnen in Waldkirch?
select count(*) from schueler join orte 
on schueler.idOrte = Orte.id where orte.name = 'Waldkirch';
-- j.	Wie viele Schüler, die bei Herrn Bohnert Unterricht haben, wohnen in Emmendingen?
select count(*) from schueler join orte 
on schueler.idOrte = Orte.id 
join lehrer_hat_schueler on schueler.id = lehrer_hat_schueler.idSchueler
join lehrer on lehrer.id = lehrer_hat_schueler.idLehrer where orte.name = 'Emmendingen' and lehrer.name = 'Bohnert';
-- k.	Wie viele Schüler unterrichtet Frau Zelawat?
select count(*) from schueler join lehrer_hat_schueler
on schueler.id = lehrer_hat_schueler.idSchueler join 
-- l.	Wie viele Schüler russischer Nationalität unterrichtet Frau Zelawat?
-- m.	Welcher Lehrer verdient am meisten? (Achtung: Falle! Überprüfen Sie Ihr Ergebnis.)