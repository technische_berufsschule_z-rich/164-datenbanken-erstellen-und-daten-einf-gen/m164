create schema db_ubs;
use db_ubs;
CREATE TABLE KundenInfo (
    ID_Kunde INT NOT NULL AUTO_INCREMENT,
    Nachname VARCHAR(50) NOT NULL,
    Kontonummer VARCHAR(5) NOT NULL,
	SWIFT INT,
    Kontostand decimal(10,2),
    Erstellt_am date,
    PRIMARY KEY (ID_Kunde)
);
drop table KundenInfo;
INSERT INTO KundenInfo Values (null,'meier',12345,12,170,'2023-12-12');

INSERT INTO KundenInfo (Nachname, Kontonummer ,Kontostand,Erstellt_am) Values ('Fritz',44564,767,'2022-12-12');

ALTER TABLE KundenInfo
ADD geaendert_am datetime;

Select ID_Kunde,Nachname,Kontonummer,Kontostand from KundenInfo
where Nachname LIKE 'KEl%' OR Kontonummer > 3322 OR Kontostand <= 100
order by Nachname, Kontostand desc;

Update KundenInfo set Kontostand = 1087
where Nachname = 'Fritz';