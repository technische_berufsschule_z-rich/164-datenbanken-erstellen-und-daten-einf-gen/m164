-- Aufgaben Select Join 
use kunden;
-- 1 Kartesisches Produkt: Hier wird alles mit alles kombiniert weil wir fk und pk nicht verbunden haben
SELECT * FROM kunden
INNER JOIN orte;
-- 2.a)	Geben Sie Name, Postleitzahl und Wohnort aller Kunden aus. Die Liste enthält den Kundennamen und den Namen des Orts, wo er wohnt.
SELECT kunden.name, orte.id_postleitzahl, orte.name from kunden 
right join orte ON orte.id_postleitzahl = kunden.fk_ort_postleitzahl; 
-- b)	Geben Sie Name und Wohnort aller Kunden aus, die die Postleitzahl 79312 haben.
SELECT kunden.name, orte.name, orte.id_postleitzahl from kunden inner join orte 
on kunden.fk_ort_postleitzahl = orte.id_postleitzahl where orte.id_postleitzahl = 79312;
-- c)	Geben Sie Name und Wohnort aller Kunden aus, die in Emmendingen wohnen (Einschränkungskriterium ist 
-- NICHT die Postleitzahl, sondern 'Emmendingen').
select kunden.name , orte.name from kunden inner join orte 
on kunden.fk_ort_postleitzahl = orte.id_postleitzahl where orte.name = 'Emmendingen';
-- d)	Geben Sie Name, Wohnort und Einwohnerzahl für alle Kunden aus, die in einem Ort mit mehr als 70000 Einwohnern wohnen
select kunden.name, orte.name, orte.einwohnerzahl from kunden inner join orte 
on kunden.fk_ort_postleitzahl = orte.id_postleitzahl  where orte.einwohnerzahl > 70000;
-- e)	Geben Sie alle Orte aus, die weniger als 1000000 Einwohner haben.
select orte.name from orte where orte.einwohnerzahl < 1000000;
-- f)	Geben Sie Kundename und Ortname aus für alle Kunden, die in Orten mit einer Einwohnerzahl zwischen 100.000 und 1.500.000 leben.
select kunden.name, orte.name , orte.einwohnerzahl from kunden inner join orte on kunden.fk_ort_postleitzahl = orte.id_postleitzahl
where orte.einwohnerzahl Between 100000 AND 1500000;
-- g)	Geben Sie Kundename, Postleitzahl und Ortname aus für alle Kunden, deren Name ein "e" enthält und alle Orte,
-- die ein "u" oder ein "r" enthalten (frEd aus stUden wird also genau so angezeigt wie jEssE aus bRnz, frEd aus salzen aber nicht 
-- und martin aus hambURg auch nicht).
select kunden.name, kunden.fk_ort_postleitzahl, orte.name from kunden inner join orte on kunden.fk_ort_postleitzahl = orte.id_postleitzahl
where kunden.name LIKE '%e%' OR orte.name LIKE '%u%' OR orte.name LIKE '%r%';
