-- Aufgabe 2 
INSERT INTO tbl_schülerin VALUES (null,'Ahmad', 'Gholamsakhi', '11');
-- Aufgabe 3 
insert into tt_teilnahme values (null,'di',110,3,101 );
-- Aufgabe 4
select tbl_schülerin.Nachname as SchuelerNachname , tbl_schülerin.Vorname as SchuelerVorname, tbl_schülerin.FK_Klasse as Klasse, 
tbl_freifach.Freifach , tt_teilnahme.Tag, tt_teilnahme.Zimmer, 
tbl_lehrerin.Nachname as FFLehrerNachname, tbl_lehrerin.Vorname as FFLehrerVorname from tbl_schülerin
 INNER JOIN tt_teilnahme
ON tbl_schülerin.ID_SchülerIn = tt_teilnahme.FK_SchülerIn
inner join  tbl_freifach
on tbl_freifach.ID_Freifach = tt_teilnahme.FK_Freifach
inner join tbl_lehrerin 
on tbl_lehrerin.ID_LehrerIn = tbl_freifach.FK_FF_LehrerIn;

-- Aufgabe 5 
select Count(*) from tbl_schülerin
 INNER JOIN tt_teilnahme
ON tbl_schülerin.ID_SchülerIn = tt_teilnahme.FK_SchülerIn
inner join  tbl_freifach
on tbl_freifach.ID_Freifach = tt_teilnahme.FK_Freifach
inner join tbl_lehrerin 
on tbl_lehrerin.ID_LehrerIn = tbl_freifach.FK_FF_LehrerIn
where tbl_lehrerin.Vorname Like 'Theo' or  'Guido';
-- Aufgabe 6 nochmals anschauen 
select count(*) from tbl_schülerin 
left join tt_teilnahme on tt_teilnahme.FK_SchülerIn = tbl_schülerin.ID_SchülerIn 
where tt_teilnahme.FK_SchülerIn = null;
-- Aufgabe 7 
select count(*) from tbl_schülerin 
inner join tt_teilnahme on tt_teilnahme.FK_SchülerIn = tbl_schülerin.ID_SchülerIn 
inner join tbl_freifach on tbl_freifach.ID_Freifach = tt_teilnahme.FK_Freifach
WHERE Freifach like (SELECT Freifach FROM tbl_freifach WHERE Freifach like 'Elektronik' or 'Chor');
-- Aufgabe 8 
CREATE TABLE Anmeldungen (
    id INT NOT NULL AUTO_INCREMENT,
    Tag VARCHAR(255) NOT NULL,
    Zimmer int ,
    fkFreifach int, 
    fkSchuelerin int,
    PRIMARY KEY (id)
);
-- das ist der einzige weg wie das load data bei mir funktioniert
LOAD DATA INFILE 'Anmeldungen_LBb.csv' 
INTO TABLE Anmeldungen
FIELDS TERMINATED BY ';' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;